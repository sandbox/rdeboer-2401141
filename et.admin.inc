<?php
/**
 * @file
 * et.admin.inc - configuration options for the ET module.
 */

/**
 * Menu call back for admin configuration form.
 *
 * @param array $form
 *   Form stub.
 * @param array $form_state
 *   Form state stub.
 * @return array
 *   The complete configuration form.
 */
function et_admin_configure_form($form, &$form_state) {

  $form['#theme'] = 'system_settings_form';
  $form['#attached']['css'][] = drupal_get_path('module', 'et') . '/et.css';

  $config = variable_get('et_global_config', array());

  _et_add_ids_section($form, $config);
  _et_add_global_section($form, $config);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );
  return $form;
}

/**
 * Adds to the supplied form a section to collect account IDs.
 *
 * @param array $form
 *   The form to add to.
 * @param array $config
 *   The global configuration settings.
 */
function _et_add_ids_section(&$form, $config) {
  $keys = empty($config['keys']) ? array() : $config['keys'];

  $form['keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('ExactTarget client keys'),
    '#description' => t(''),
    '#collapsible' => FALSE,
    //'#collapsed' => !empty($keys['client_id_live']) && !empty($keys['client_secret_live']),
    '#tree' => TRUE,
  );
  $form['keys']['env'] = array(
    '#type' => 'radios',
    '#title' => t('Active environment'),
    '#options' => array(
      'test' => t('Test'),
      'live' => t('Live'),
    ),
    '#default_value' => empty($keys['env']) ? 'test' : $keys['env'],
  );
  $form['keys']['client_id_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID (test)'),
    '#default_value' => empty($keys['client_id_test']) ? '' : $keys['client_id_test'],
    '#size' => 40,
    '#states' => array(
      'visible' => array(':input[name="keys[env]"]' => array('value' => 'test')),
    ),
  );
  $form['keys']['client_secret_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret (test)'),
    '#default_value' => empty($keys['client_secret_test']) ? '' : $keys['client_secret_test'],
    '#size' => 40,
    '#states' => array(
      'visible' => array(':input[name="keys[env]"]' => array('value' => 'test')),
    ),
  );
  $form['keys']['client_id_live'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID (<em>live</em>)'),
    '#default_value' => empty($keys['client_id_live']) ? '' : $keys['client_id_live'],
    '#size' => 40,
    '#states' => array(
      'visible' => array(':input[name="keys[env]"]' => array('value' => 'live')),
    ),
  );
  $form['keys']['client_secret_live'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret (<em>live</em>)'),
    '#default_value' => empty($keys['client_secret_live']) ? '' : $keys['client_secret_live'],
    '#size' => 40,
    '#states' => array(
      'visible' => array(':input[name="keys[env]"]' => array('value' => 'live')),
    ),
  );
}

/**
 * Adds the ET client library location section.
 *
 * @param array $form
 *   The form to add to.
 * @param array $config
 *   The global configuration settings.
 */
function _et_add_global_section(&$form, $config) {
  global $base_url;

  $global = empty($config['global']) ? array() : $config['global'];
  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('ExactTarget global configuration'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['global']['lib'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget library to use'),
    '#field_prefix' => "$base_url/",
    '#description' => t('Defaults to %lib', array('%lib' => ET_DEF_CLIENT_LIB)),
    '#default_value' => empty($global['lib']) ? '' : $global['lib'],
  );
}
/**
 * Validate the configuration form submission and display errors.
 *
 * @param array $form
 *   The submitted configuration form.
 * @param array $form_state
 *   The submitted configuration form state.
 */
function et_admin_configure_form_validate(&$form, &$form_state) {

  // Remove unnecessary elements from the form values. Used for submit too.
  form_state_values_clean($form_state);

  $keys = $form_state['values']['keys'];
  $env = $keys['env'];
  if (strlen($keys["client_id_$env"]) < 5 || strlen($keys["client_secret_$env"]) < 5) {
    drupal_set_message(t('Your ExactTarget %env environment is active, but the keys for %env are invalid.', array('%env' => $env)), 'warning');
  }

  $lib = $form_state['values']['global']['lib'];
  if (!empty($lib) && !file_exists($lib)) {
    drupal_set_message(t('The file %lib could not be found.', array('%lib' => $lib)), 'error');
  }
}

/**
 * Save the values on the configuration form to the database.
 *
 * @param array $form
 *   The submitted configuration form.
 * @param array $form_state
 *   The submitted configuration form state.
 */
function et_admin_configure_form_submit($form, &$form_state) {
  $config = $form_state['values'];
  variable_set('et_global_config', $config);
  drupal_set_message(t('The ExactTarget configuration was saved.'));
}
