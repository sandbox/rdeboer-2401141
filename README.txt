
ET - salesforce ExactTarget API for Data Extensions (in Email application)
==========================================================================

A small module to perform read, write and delete operations on ExactTarget
Data Extensions.

Based on the ExactTraget ET_Client SOAP API. At the time of writing, 2015, the
REST API did not support access to Data Extensions.

Download the Fuel SDK from https://github.com/ExactTarget/FuelSDK-PHP

Extract it to sites/all/libraries, renaming the top directory to fuelsdk so that
the path to ET_Client.php becomes sites/all/libraries/fuelsdk/ET_Client.php
If you wish to use a different library path, you can, but you must tell the
module on its configuration page, admin/config/system/exacttarget.

Make sure you do NOT have a fuelsdk/config.php file as this is not a safe
location for it AND the config.php contents will override any client keys you
type through the module configuration page.

In addition, you need to drop the WSDL, ExactTargetWSDL.xml, into your site
root. You can get a copy from here:
https://github.com/ExactTarget/dataExtensionEditor-php/blob/master/cache/ExactTargetWSDL.xml

Obtain client keys from
https://code.exacttarget.com/apis-sdks/soap-api/using-app-center-to-get-an-api-key.html
and enter these on the ET module configuration page.

After this you may use any of these CRUD-style API functions:

  et_get_rows($data_ext_key, $fields, $filter = NULL)
  et_create_row($data_ext_key, $fields)
  et_update_row($data_ext_key, $fields)
  et_update_or_create_row($data_ext_key, $fields, $primary_key)
  et_delete_row($data_ext_key, $fields)

The string $data_ext_key is known as the External Key in the ExactTarget UI.

$fields is an array of either field names or field values keyed by field names.
One of the fields in each of the calls should be the primary key field.
Typically this is an email field. See the code doc for details.

You do not need to explicitly initialise or re-initialise the ExactTarget client
connection. The module automatically takes care of keeping the connection alive
until it expires after which it will automatically create a new connection.

Rules
-----
The Rules action et_action_update_or_create_row is available to invoke
the API function et_update_or_create_row(), mentioned above. This allows you
to configure rules that update an ExactTarget Data Extension in response to
user logins, product purchases, user account updates etc.
