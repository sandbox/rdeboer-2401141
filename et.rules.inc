<?php
/**
 * @file
 * Rules Actions for ExactTarget Data Extension integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function et_rules_action_info() {
  $actions = array();

  $actions['et_action_update_or_create_row'] = array(
    'label' => t('Update or create a row in the Data Extension'),
    'parameter' => array(
      'data_ext_key' => array(
        'type' => 'text',
        'label' => t('ExactTarget Data Extension external key. Typically something like ...-...-...-...-...'),
      ),
      'fields' => array(
        'type' => 'list<text>',
        'label' => t('The data fields to update, as name=value pairs'),
      ),
      'primary_key' => array(
        'type' => 'text',
        'label' => t('Name of the field used to determine if the row already exists.'),
      ),
    ),
    'group' => t('ExactTarget Data Extension interface'),
    'callbacks' => array(
      'execute' => 'et_action_update_or_create_row',
    ),
  );

  return $actions;
}

/**
 * Update a row in the Data Extension, identifed by the external key.
 *
 * The $fields array must include the primary key field, e.g. email address.
 *
 * @param string $data_ext_key, Data Extension external key
 * @param array $fields,
 * @param string $primary_key, field name of the unique row identifier
 */
function et_action_update_or_create_row($data_ext_key, $fields, $primary_key) {
  $trimmed_fields = array();
  foreach ($fields as $name_value) {
    list($name, $value) = explode('=', $name_value, 2);
    $name = trim($name);
    if (!empty($name) && !empty($value)) {
      $trimmed_fields[$name] = trim($value);
    }
  }

  $result = et_update_or_create_row($data_ext_key, $trimmed_fields, $primary_key);

  if (empty($result['status'])) {
    drupal_set_message(t('So sorry! Submission to our Marketing Automation System failed with the error shown below. Please try again later.'), 'error');
    drupal_set_message($result['message'], 'error');
  }
}
