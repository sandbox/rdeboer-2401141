<?php
/**
 * @file
 * et.class.inc - class ET (ExactTarget) to perform the basic CRUD operations.
 *
 * @see https://code.exacttarget.com/apis-sdks/fuel-sdks/php/getting-started-with-the-php-sdk.html
 */

require_once et_get_lib();

/**
 * ExactTarget API class.
 *
 * Currently only Data Extension Row operations are implemented.
 * Please add more!
 */
class ET {

  private $_et_client;

  /**
   * ExactTarget client constructor.
   */
  public function __construct() {
    // Nothing to do.
  }

  /**
   * Refreshes or requests a new connection, using the credentials supplied.
   *
   * @return bool, TRUE if all is ok
   */
  public function init($debug = FALSE, $config = array()) {

    if (isset($this->_et_client)) {
      return TRUE;
    }

    try {
      // Note the presence of a config.php file overrides the $config parameter
      // passed in.
      $this->_et_client = new ET_Client(FALSE, $debug, $config);
    }
    catch (Exception $ex) {
      $this->log($ex->getMessage(), WATCHDOG_ERROR);
      $this->_et_client = NULL;
      return FALSE;
    }
    return isset($_et_client);
  }

  /**
   * Get the response error message
   */
  public function log($message, $severity = WATCHDOG_NOTICE) {
    watchdog('ExactTarget', $message, $severity);
  }

  /**
   * Initialises a DataExtension Row object.
   *
   * @param string $data_ext_key, aka  External Key
   * @param array $fields, names of the fields of interest
   *
   * @return \ET_DataExtension_Row
   */
  private function initDataExtension($data_ext_key, $fields) {
    $data_ext_row = new ET_DataExtension_Row();

    $data_ext_row->authStub = $this->_et_client;
    $data_ext_row->CustomerKey = $data_ext_key;
    $data_ext_row->props = $fields;

    return $data_ext_row;
  }

  /**
   * Pull a row from the supplied DataExtension.
   *
   * @param string $data_ext_key
   * @param array $fields, the field names to pull
   * @param array $filter, optional filter
   *   Example:
   *    array(
   *      'Property' => 'email',
   *      'SimpleOperator' => 'equals',
   *      'Value' => 'someone@company.com');
   *
   * @return array $result
   * - int 'code', normally 200 on success
   * - bool 'status'
   * - string 'message', normally empty on success
   * - array 'result', rows of arrays of field values
   */
  public function getDataExtensionRow($data_ext_key, $fields, $filter = NULL) {

    $data_ext_row = $this->initDataExtension($data_ext_key, $fields);
    if (!empty($filter)) {
      $data_ext_row->filter = $filter;
    }
    $result = $data_ext_row->get();

    $rows = array();
    if (!empty($result->results)) {
      foreach ($result->results as $row_num => $properties) {
        $row = array();
        foreach ($properties->Properties->Property as $property) {
          $row[] = $property->Value;
        }
        $rows[$row_num] = $row;
      }
    }
    return array(
      'code' => $result->code,
      'status' => $result->status,
      'message' => isset($result->message) ? $result->message : '',
      'result' => $rows,
    );
  }

  /**
   * Push a new row into the supplied DataExtension.
   *
   * @param string $data_ext_key
   * @param array $fields, the field values to push, indexed by field name
   *
   * @return array $result
   * - int 'code', normally 200 on success
   * - bool 'status'
   * - string 'message', normally empty on success
   * - array 'result', result[0] may contain error info when status==FALSE
   *
   * Reasons why an insert may fail include violation of the primary key field.
   * You typically cannot insert a row with the same emai address twice. Use
   * updateDataExtensionRow for that.
   */
  function createDataExtensionRow($data_ext_key, $fields) {

    $data_ext_row = $this->initDataExtension($data_ext_key, $fields);
    $result = $data_ext_row->post();

    return array(
      'code' => $result->code,
      'status' => $result->status,
      'message' => isset($result->message) ? $result->message : (isset($result->results[0]->ErrorMessage) ? $result->results[0]->ErrorMessage : ''),
      'result' => $result->results,
    );
  }

  /**
   * Patch an existing row in the supplied DataExtension.
   *
   * @param string $data_ext_key
   * @param array $fields, the field values to be patched, indexed by field name
   *   make sure you include the primary key field
   *
   * @return array $result
   * - int 'code', normally 200 on success
   * - bool 'status'
   * - string 'message', normally empty on success
   * - array 'result', result[0] may contain error info when status==FALSE
   *
   * Reasons why an insert may fail include violation of the primary key field.
   * You typically cannot insert a row with the same emai address twice. Use
   * updateDataExtensionRow for that.
   */
  function updateDataExtensionRow($data_ext_key, $fields) {

    $data_ext_row = $this->initDataExtension($data_ext_key, $fields);
    $result = $data_ext_row->patch();

    return array(
      'code' => $result->code,
      'status' => $result->status,
      'message' => isset($result->message) ? $result->message : (isset($result->results[0]->ErrorMessage) ? $result->results[0]->ErrorMessage : ''),
      'result' => $result->results,
    );
  }

  /**
   * Delete an existing row from the supplied DataExtension.
   *
   * @param string $data_ext_key
   * @param array $fields, the field values to be patched, indexed by field name
   *   make sure you include the primary key field
   *
   * @return array $result
   * - int 'code', normally 200 on success
   * - bool 'status'
   * - string 'message', normally empty on success
   * - array 'result', result[0] may contain error info when status==FALSE
   */
  function deleteDataExtensionRow($data_ext_key, $fields) {

    $data_ext_row = $this->initDataExtension($data_ext_key, $fields);
    $result = $data_ext_row->delete();

    return array(
      'code' => $result->code,
      'status' => $result->status,
      'message' => isset($result->message) ? $result->message : (isset($result->results[0]->ErrorMessage) ? $result->results[0]->ErrorMessage : ''),
      'result' => $result->results,
    );
  }
}
